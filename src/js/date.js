(function ($) {
    $(document).ready(function () {
        var date = new Date();
        $('#date').text(date.getFullYear());
    });
}(jQuery));