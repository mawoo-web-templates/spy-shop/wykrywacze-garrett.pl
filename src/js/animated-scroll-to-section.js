(function ($) {
    $(window).on('load', function () {
        var scroll = {
            windowWidth: function () {
                return $(window).width();
            },
            offset: {
                mobile: 55,
                desktop: 62
            },
            changeUri: function () {
                $('a[data-scroll="section-another-page"]').each(function () {
                    var str = $(this).attr('href');
                    var hash = str.substr(str.search('#') + 1, str.length - 1);
                    var newURI = function () {
                        var parameters = {
                            section: hash
                        };
                        return str.replace('#' + hash, '') + '?' + $.param(parameters);
                    };
                    $(this).attr('href', newURI());
                });
            },
            animateToSection: function (hash) {
                this.closeMenu();
                var offset;
                if (this.windowWidth() < 1200) {
                    offset = this.offset.mobile;
                } else if (this.windowWidth() >= 1200) {
                    offset = this.offset.desktop;
                }
                $('html, body').animate({
                    scrollTop: $('#' + hash).offset().top - offset
                }, 500, function () {
                    $(this).clearQueue();
                });
            },
            closeMenu: function () {
                $('#navigation .main-menu, #navigation .navigation__hamburger-menu').removeClass('active');
            }
        }

        if (window.location.search) {
            var str = window.location.search.replace('?section=', '');
            setTimeout(function () {
                scroll.animateToSection(str);
            }, 0);
        }

        if ($('a[data-scroll="section-another-page"]').length > 0) {
            scroll.changeUri();
        }

        $('body').on('click', 'a[data-scroll="section-page"]', function (event) {
            event.preventDefault();
            var str = $(this).attr('href');
            var hash = str.substr(str.search('#') + 1, str.length - 1);
            scroll.animateToSection(hash);
        });
    });
}(jQuery));