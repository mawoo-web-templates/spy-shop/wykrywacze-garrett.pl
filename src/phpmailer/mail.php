<?php

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Form fields
    $name              = test_input($_POST['name']);
    $email             = test_input($_POST['email']);
    $phone             = test_input($_POST['phone']);
    $subject           = test_input($_POST['subject']);
    $message           = test_input($_POST['message']);

    // reCAPTCHA widget
    $userIP            = $_SERVER["REMOTE_ADDR"];
    $gRecaptchaResponse = $_POST['g-recaptcha-response'];
    $secretKey         = '';
    $request           = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secretKey}&response={$gRecaptchaResponse}&remoteip={$userIP}");

    if (!strstr($request, "true")) {
        echo 'grecaptcha-false';
    } else {
        require 'PHPMailerAutoload.php';

        $mail = new PHPMailer;

        // Enable verbose debug output
        //$mail->SMTPDebug = 3;

        // Set mailer to use SMTP
        $mail->isSMTP();

        // Specify main and backup SMTP servers
        $mail->Host       = '';

        // Enable SMTP authentication
        $mail->SMTPAuth   = true;

        // SMTP username
        $mail->Username   = '';

        // SMTP password
        $mail->Password   = '';

        // Enable TLS encryption, `ssl` also accepted
        $mail->SMTPSecure = 'tls';

        // TCP port to connect to
        $mail->Port       = 587;

        $mail->setFrom($email, $name);
        $mail->addAddress('', ''); // Add a recipient
        //$mail->addAddress('ellen@example.com');               // Name is optional
        //$mail->addReplyTo('info@example.com', 'Information');
        //$mail->addCC('cc@example.com');
        //$mail->addBCC('bcc@example.com');

        //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        $mail->isHTML(true); // Set email format to HTML
        $mail->CharSet = 'UTF-8'; // The character set of the message.

        $mail->Subject = 'Wiadomość ze strony wykrywacze-garrett.pl';
        $mail->Body    = '<p>Numer telefonu:<br />' . $phone . '</p><p>Wiadomość:<br />' . $message . '</p>';
        $mail->AltBody = '';

        if (!$mail->send()) {
            echo 'phpmailer-error';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            echo 'phpmailer-success';
        }
    }
}
